var Router = require('express').Router();

//import Router from 'express'.Router();

import {registerValidation} from '../validation/validation';

import con from '../database/db';

// Admin is getting registered 
Router.post('/admin',(req: any,res: any)=>{
    // Assuming name, password is asked from the user at the time of registering
    let name:string = req.body.name;
    let password:any = req.body.password;

    // Make sure that both the entries have been given
    if(name && password){
        // check whether they have come in the correct format using joi
        let validation = registerValidation(req.body);
        if(validation.error){
            res.status(400).send(validation.error.details[0].message);
        }else{
            con.query('select password from register_user where name = ?',[name],(err:any,result:any)=>{
                if(result.length>0){
                    res.send('user already registered');
                    // Redirect user to the login page
                }else{
                    con.query('insert into register_user values ("'+name+'","'+password+'")',(err:any)=>{
                        if(!err){
                            res.send('user registered');
                        }else{
                            res.send(`User not registerd due to ${err}`);
                        }
                    })
                }
            })

            
        }
    }else{  
        res.send('Name or password not provided');
    }
    
});