const express = require('express');
const mysql = require('mysql');
require('dotenv/config');
const app = express();

// Import Routes
const user_register = require('./routes/route');

// Middlewares 
app.use(express.json);  // Bodyparser for the json data
app.use('/register',user_register);


app.listen('8080',()=>{
    console.log('server at 8080');
});
