const mysql = require('mysql');

const con = mysql.createConnection({
    user:'root',
    host:'localhost',
    password:process.env.db_password,
    database: 'user'
});

con.connect((error)=>{
    if(error){
        console.log(`Error in connecting the database ${error}`);
    }else{
        console.log('Database connected..');
    }
});

module.exports = con;
