import { object, string } from '@hapi/joi';
// Use object directly in place of Joi.object and string instead of Joi.string
// Comment new
const registerValidation = (data)=>{
    const schema = object({
        name: string().max(10).min(3).required(), 
        password: string().max(10).min(5).required()
    });
    return schema.validate(data);
};

const _registerValidation = registerValidation;
export { _registerValidation as registerValidation };